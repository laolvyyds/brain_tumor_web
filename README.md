# braintumor

## 文件说明

### component： 静态组件包

1. AsideMenu： 侧边栏

### pages：路由组件包

1. LogIn：登录页面

2. HomePage：主页

3. Map：地图页面

4. Statistic：统计页面

5. UserOptions：用户管理页面

6. LogInManage：登录管理

7. OperationDialog：操作日志

8. PowerManage：权限管理

9. FirmManage：企业管理

10. TextEdit：发布