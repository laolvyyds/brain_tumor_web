// 引入 vue-router
import VueRouter from 'vue-router'

// 引入组件
import LogInManage from "@/pages/LogInManage";
import Statistic from "@/pages/Statistic";
import UserOperation from "@/pages/UserOperation";
import Map from "@/pages/Map";
import FirmManage from "@/pages/FirmManage";
import TextEdit from "@/pages/TextEdit";
import OperationDialog from "@/pages/OperationDialog";
import PowerManage from "@/pages/PowerManage";
import ForgetPassword from "@/pages/ForgetPassword";
import HomePage from "@/pages/HomePage";
import LogIn from "@/pages/LogIn";
import UserData from "@/pages/UserData";
import MedicalRecord from "@/pages/MedicalRecord";

// 创建路由
export default new VueRouter({

    routes:[
        {
            path: '/',
            component: LogIn
        },
        {
            name:'homepage',
            path:'/homepage',
            component:HomePage,
            children: [
                {
                    name:`loginmanage`,
                    path:'loginmanage',
                    component:LogInManage
                },
                {
                    name:'statistic',
                    path:'statistic',
                    component:Statistic
                },
                {
                    name:'useropeartion',
                    path:'useropeartion',
                    component:UserOperation
                },
                {
                    name:'map',
                    path:'map',
                    component:Map
                },
                {
                    name:'firmmanage',
                    path:'firmmanage',
                    component:FirmManage
                },
                {
                    name:'textedit',
                    path:'textedit',
                    component:TextEdit
                },
                {
                    name:'operationdialog',
                    path:'operationdialog',
                    component:OperationDialog
                },
                {
                    name:'powermanage',
                    path:'powermanage',
                    component:PowerManage
                },
                {
                    name: 'userdata',
                    path: 'userdata',
                    component: UserData,
                },
                {
                    name: 'medicalrecord',
                    path: 'medicalrecord',
                    component: MedicalRecord
                }
            ]
        },
        {
            name:'fogetpassword',
            path:'/forgetpassword',
            component:ForgetPassword
        }

    ]
});

